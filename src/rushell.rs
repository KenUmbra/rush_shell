use std::env;
use std::io;
use std::io::Write;
use std::process::Command;
use colored::Colorize;
use crate::unix_metadata;
use crate::builtins;

/// This function creates the prefix before user input.
fn rush_prefix() {
    print!("{}{}{}:{}\n{} ", 
             unix_metadata::get_username().red().bold(),
             "@".red().bold(),
             unix_metadata::get_hostname().red().bold(),
             format!("{}", env::current_dir().unwrap().display()).yellow().bold(),
             ">".purple());
    io::stdout().flush().unwrap();
}

/// This function reads a line from stdin, and strips the special characters from it.
/// output: The stripped line.
fn rush_read() -> String {
    let mut line = String::new();

    io::stdin().read_line(&mut line).expect("Bad Input");
    if let Some('\n') = line.chars().next_back() {
        line.pop();
    }
    if let Some('\r') = line.chars().next_back() {
        line.pop();
    }
    
    line
}

/// This function parses the arguments into a vector.
/// input: The line read from stdin.
/// output: The vector containing the arguments.
fn rush_split(line: String) -> Vec<String> {
    let split_line: Vec<&str> = line.split(' ').collect();
    let mut args = Vec::new();
    let mut arg = String::new();
    let mut sep: char = ' ';
    
    for i in 0..split_line.len() {
        let first_char: char;
        let last_char: char;
        let tmp = split_line.get(i).unwrap();
        if tmp.chars().nth(0) == None
        {
            break;
        }
        
        arg.push_str(tmp);
        if (tmp.matches('"').count() != 2 && tmp.chars().nth(0).unwrap() == '"') || (tmp.matches('\'').count() != 2 && tmp.chars().nth(0).unwrap() == '\'')
        {
            arg.push_str(" ");
            sep = tmp.chars().nth(0).unwrap();
            continue;
        }
        else if sep != ' ' && tmp.chars().rev().nth(0).unwrap() != sep
        {
            arg.push_str(" ");
            continue;
        }

        first_char = arg.chars().nth(0).unwrap();
        last_char = arg.chars().rev().nth(0).unwrap(); 
        if sep != ' ' || (first_char == last_char && (first_char == '"' || first_char == '\'')) { 
            arg = (&arg[1..(arg.len() - 1)]).to_string();
        }

        args.push(arg);
        arg = String::new();
        sep = ' ';
    }
    
    args
}

/// This function executes the command.
/// input: The vector containing the arguments to be executed.
fn rush_exec(args: Vec<String>) {
    if args.get(0) == None
    {
        return;
    }
    
    let arg0 = args.get(0).unwrap();
    let arg0_str = &arg0[..];
    let rest = &args[1..]; 
    
    if !builtins::nav_folders(arg0) {
        match arg0_str {
            "cd" => { 
                let arg1: String;
                
                if rest.get(0) != None {
                    arg1 = String::from(rest.get(0).unwrap());
                }
                else {
                    arg1 = String::from("~");
                }
                
                if !builtins::nav_folders(&arg1) {
                    println!("cd: no such file or directory: {}", arg1);
                }
            },
            _ => {
                let child_res = Command::new(arg0).args(rest).spawn();

                if child_res.is_ok()
                {
                    let _ = child_res.unwrap().wait(); 
                }
                else
                {
                    println!("rush: command not found: {}", arg0);
                }
            },
        }
    }
}

/// This function is the main loop of the shell.
pub fn rush() {
    let mut line: String;
    let mut args: Vec<String>;

    loop {
        rush_prefix();
        line = rush_read();
        args = rush_split(line);
        rush_exec(args);
    }
}

