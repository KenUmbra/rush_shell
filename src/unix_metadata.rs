use libc::{c_char, sysconf, _SC_HOST_NAME_MAX};
use std::os::unix::ffi::OsStringExt;
use std::ffi::OsString;
use std::io::Error;
use std::os::unix::fs::MetadataExt;
use std::fs;

/// This function gets the machine's hostname.
/// output: The machine's hostname.
pub fn get_hostname() -> String {
    let end;
    let hostname_max = unsafe { sysconf(_SC_HOST_NAME_MAX) };
    let mut buffer = vec![0; (hostname_max as usize) + 1];
    let returncode = unsafe { libc::gethostname(buffer.as_mut_ptr() as *mut c_char, buffer.len()) };
    
    if returncode != 0 {
        panic!("gethostname failed: {}", Error::last_os_error());
    }
    
    end = buffer.iter().position(|&b| b == 0).unwrap_or(buffer.len());
    buffer.resize(end, 0);
    OsString::from_vec(buffer).into_string().unwrap()
}

/// This function gets the current user's username.
/// output: The user's username.
pub fn get_username() -> String {
    let passwd = fs::read_to_string("/etc/passwd").unwrap();
    let split_passwd: Vec<&str> = passwd.split("\n").collect();
    let proc_uid = std::fs::metadata("/proc/self").map(|m| m.uid()).unwrap();
    
    for i in &split_passwd {
        let split_entry: Vec<&str> = i.split(":").collect();
        if split_entry.len() > 3 {
            if split_entry.get(2).unwrap().parse::<u32>().unwrap() == proc_uid {
                return split_entry.get(0).unwrap().to_string();
            }
        }
    }
    
    String::from("unknown")
}
