use std::env;
use std::path::Path;
use dirs::{home_dir};

/// This function adjusts the current working directory.
/// input: The path to the destination.
/// output: Whether the operation was successful or not.
pub fn nav_folders(dst: &String) -> bool {
    let mut new_dst: String; 
    let mut path = Path::new(dst);
    
    if dst.chars().nth(0).unwrap() == '~' {
        new_dst = home_dir().unwrap().into_os_string().into_string().unwrap();
        new_dst.push_str(&dst[1..]);
        path = Path::new(&new_dst);
    }

    env::set_current_dir(&path).is_ok() 
}
