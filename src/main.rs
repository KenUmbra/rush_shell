mod unix_metadata;
mod rushell;
mod builtins;
use std::env;

fn main() { 
    env::set_var("SHELL", std::env::current_exe().unwrap());
    rushell::rush();
}
