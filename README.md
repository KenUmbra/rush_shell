# Rush

## Name
Rush Shell

## Description
A shell I made in the Rust programming language in order to learn it.

## Badges
--

## Visuals
--

## Installation
1. Make sure you have rust & cargo installed.
2. Clone the repository:
```
git clone https://gitlab.com/KenUmbra/rush_shell.git
```
3. cd into the folder.
3. Build the executable:
```
cargo build --release
```
4. The executable should be inside `./target/release`
5. Move it to `/usr/bin`
6. Enjoy!


## Usage
To be used as an alternative to existing shells.

## Support
You can reach me via discord, I go by Kèn#4248.
If I enable issues then you can also use that.

## Roadmap
1. Add pipe support (for example: `command1 | command2`).
2. Add logical function support (for example: `command1 || command2 && (command3; command4)`). 
3. Add nested commands support (for example: `$(command)`).
3. Improve navigation (support for directional movement & history).
4. Add autocompletion.
4. Add a customizable config file (includes aliases and settings).
5. Add install script.
6. Replace existing builtins (such as `ls`) with custom ones.

## Contributing
--

## Authors and acknowledgment
I have made this on my own, using the rust documentation.

## License
GPL3.

## Project status
In progress.
